import {
  ADD_TASK,
  CHANGE_THEME,
  DELETE_TASK,
  DONE_TASK,
  UNDONE_TASK,
  EDIT_TASK,
  UPDATE_TASK,
} from "../constants/ToDoListConstant";

export const addTaskAction = (newTask) => ({
  type: ADD_TASK,
  payload: newTask,
});

export const changeTheme = (themeId) => ({
  type: CHANGE_THEME,
  payload: themeId,
});

export const doneTaskAction = (taskId) => ({
  type: DONE_TASK,
  payload: taskId,
});
export const unDoneTaskAction = (taskId) => ({
  type: UNDONE_TASK,
  payload: taskId,
});

export const delTaskAction = (taskId) => ({
  type: DELETE_TASK,
  payload: taskId,
});

export const editTaskAction = (task) => ({
  type: EDIT_TASK,
  payload: task,
});

export const updateTaskAction = (taskName) => ({
  type: UPDATE_TASK,
  payload: taskName,
});
