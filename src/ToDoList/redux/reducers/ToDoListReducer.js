import { arrTheme } from "../../Themes/ThemeManage";
import { ToDoListDarkTheme } from "../../Themes/ToDoListDarkTheme";
import {
  ADD_TASK,
  CHANGE_THEME,
  DELETE_TASK,
  DONE_TASK,
  EDIT_TASK,
  UNDONE_TASK,
  UPDATE_TASK,
} from "../constants/ToDoListConstant";

const initialState = {
  theme: ToDoListDarkTheme,
  taskList: [
    { id: 1, taskName: "Dàn Layout", done: true },
    { id: 2, taskName: "Code css", done: true },
    { id: 3, taskName: "Code javascript", done: false },
    { id: 4, taskName: "Code react", done: false },
  ],
  taskEdit: { id: 4, taskName: "react", done: false },
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TASK: {
      if (payload.taskName.trim() == "") {
        alert("Please type Task name!");
        return { ...state };
      }
      let index = state.taskList.findIndex(
        (task) => task.taskName == payload.taskName
      );
      let cloneTaskList = [...state.taskList];

      //kiểm tra dữ liệu trùng
      if (index != -1) {
        alert("Task name is alreay exist");
        return { ...state };
      } else {
        let newTask = { ...payload };
        cloneTaskList.push(newTask);
      }

      state.taskList = cloneTaskList;
      return { ...state };
    }
    case CHANGE_THEME: {
      let index = arrTheme.findIndex((theme) => theme.id == payload);

      if (index != -1) {
        state.theme = arrTheme[index].theme;
      }
      return { ...state };
    }

    case DONE_TASK: {
      let cloneTaskList = [...state.taskList];
      let index = cloneTaskList.findIndex((task) => task.id == payload);
      if (index !== -1) {
        cloneTaskList[index].done = true;
      }

      return { ...state, taskList: cloneTaskList };
    }

    case UNDONE_TASK: {
      let cloneTaskList = [...state.taskList];
      let index = cloneTaskList.findIndex((task) => task.id == payload);
      if (index !== -1) {
        cloneTaskList[index].done = false;
      }

      return { ...state, taskList: cloneTaskList };
    }

    case DELETE_TASK: {
      let cloneTaskList = [...state.taskList];
      cloneTaskList = cloneTaskList.filter((task) => task.id !== payload);
      return { ...state, taskList: cloneTaskList };
    }

    case EDIT_TASK: {
      return { ...state, taskEdit: payload };
    }

    case UPDATE_TASK: {
      state.taskEdit = { ...state.taskEdit, taskName: payload };
      let cloneTaskList = [...state.taskList];
      let index = cloneTaskList.findIndex(
        (task) => task.id == state.taskEdit.id
      );

      if (index !== -1) {
        cloneTaskList[index] = state.taskEdit;
      }

      state.taskList = cloneTaskList;
      return { ...state };
    }

    default:
      return { ...state };
  }
};
