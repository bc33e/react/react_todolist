import React, { Component } from "react";
import { Container } from "../ToDoList/Components/Container";
import { ThemeProvider } from "styled-components";
import { connect } from "react-redux";
import { Dropdown } from "../ToDoList/Components/Dropdown";
import {
  Heading1,
  Heading2,
  Heading3,
  Heading4,
  Heading5,
} from "../ToDoList/Components/Heading";
import { Label, TextField } from "../ToDoList/Components/TextField";
import { Button } from "../ToDoList/Components/Button";
import { Table, Tr, Td, Th, Thead, Tbody } from "../ToDoList/Components/Table";
import {
  addTaskAction,
  changeTheme,
  doneTaskAction,
  unDoneTaskAction,
  delTaskAction,
  editTaskAction,
  updateTaskAction,
} from "./redux/actions/ToDoListAction";
import { arrTheme } from "../ToDoList/Themes/ThemeManage";

class ToDoList extends Component {
  state = {
    taskName: "",
    disabled: true,
  };
  renderTaskToDo = () => {
    return this.props.taskList
      .filter((task) => !task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              {/* edit button */}
              <Button
                className="ml-1"
                onClick={() => {
                  this.props.dispatch(editTaskAction(task));
                  this.setState({ disabled: false });
                }}
              >
                <i className="fa fa-edit text-primary"></i>
              </Button>
              {/* check button */}
              <Button
                className="ml-1"
                onClick={() => {
                  this.props.dispatch(doneTaskAction(task.id));
                }}
              >
                <i className="fa fa-check text-success"></i>
              </Button>
              {/* delete button */}
              <Button
                className="ml-1"
                onClick={() => {
                  this.props.dispatch(delTaskAction(task.id));
                }}
              >
                <i className="fa fa-trash text-danger"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderTaskCompleted = () => {
    return this.props.taskList
      .filter((task) => task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                className="ml-1"
                onClick={() => {
                  this.props.dispatch(unDoneTaskAction(task.id));
                }}
              >
                <i className="fa fa-times text-warning"></i>
              </Button>
              <Button
                className="ml-1"
                onClick={() => {
                  this.props.dispatch(delTaskAction(task.id));
                }}
              >
                <i className="fa fa-trash text-danger"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  handleChangeInput = (e) => {
    console.log(this.state);
    let { name, value } = e.target;
    this.setState({ [name]: value });
  };

  renderTheme = () => {
    return arrTheme.map((theme, index) => {
      return (
        <option value={theme.id} key={index}>
          {theme.name}
        </option>
      );
    });
  };

  render() {
    return (
      <ThemeProvider theme={this.props.theme}>
        <Container className="w-50 py-4">
          <Dropdown
            onChange={(e) => {
              let { value } = e.target;
              this.props.dispatch(changeTheme(value));
            }}
          >
            {this.renderTheme()}
          </Dropdown>
          <Heading3 className="text-center">To do List</Heading3>
          <TextField
            // onChange={(e) => {
            //   this.setState({ taskName: e.target.value });
            // }}
            value={this.state.taskName}
            onChange={this.handleChangeInput}
            name="taskName"
            label="Task name"
            className="w-50"
          />
          <Button
            disabled={this.state.disabled == true ? false : true}
            onClick={() => {
              //lấy thông tin từ input
              let { taskName } = this.state;
              //tạo id ngẫu nhiên
              let newTask = {
                id: Date.now(),
                taskName: taskName,
                done: false,
              };
              console.log("newTask: ", newTask);
              this.setState({ taskName: "" }, () => {
                this.props.dispatch(addTaskAction(newTask));
              });
            }}
            className="ml-2"
          >
            <i className="fa fa-plus mr-2 "></i>Add
          </Button>

          <Button
            className="ml-2 text-info"
            disabled={this.state.disabled == true ? true : false}
            onClick={() => {
              let { taskName } = this.state;
              this.setState(
                {
                  taskName: "",
                  disabled: true,
                },
                () => {
                  this.props.dispatch(updateTaskAction(taskName));
                }
              );
            }}
          >
            <i className="fa fa-upload mr-2"></i>Update
          </Button>
          <hr />
          <Heading4>Task to do</Heading4>
          <Table>
            <Thead>{this.renderTaskToDo()}</Thead>
          </Table>
          <hr />
          <Heading4>Task completed</Heading4>
          <Table>
            <Thead>{this.renderTaskCompleted()}</Thead>
          </Table>
        </Container>
      </ThemeProvider>
    );
  }

  // Life cycle trả về prop và state cũ trước khi render nhưng chạy sau render
  componentDidUpdate(preProps, preState) {
    //so sánh props (taskedit khác với task hiện tại)
    if (preProps.taskEdit.id !== this.props.taskEdit.id)
      this.setState({
        taskName: this.props.taskEdit.taskName,
      });
  }
}

let mapStateToProps = (state) => {
  return {
    theme: state.ToDoListReducer.theme,
    taskList: state.ToDoListReducer.taskList,
    taskEdit: state.ToDoListReducer.taskEdit,
  };
};

export default connect(mapStateToProps)(ToDoList);
