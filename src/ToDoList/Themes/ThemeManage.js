import { ToDoListDarkTheme } from "./ToDoListDarkTheme";
import { ToDoListLightTheme } from "./ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "./ToDoListPrimaryTheme";

export const arrTheme = [
  { id: 1, name: "Dark", theme: ToDoListDarkTheme },
  { id: 2, name: "Light", theme: ToDoListLightTheme },
  { id: 3, name: "Primary", theme: ToDoListPrimaryTheme },
];
